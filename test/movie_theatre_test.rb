require "test_helper"

class MovieTheatreTest < Minitest::Test
  def test_auditorium_should_initiate_with_given_seat_scheme
    seats = [['A', 9], ['B', 6], ['C', 7]]
    pricing = { 'Platinum': 320, 'Gold': 280, 'Silver': 240 }.freeze
    audi = ::Auditorium.new(seats, pricing, 1)

    seats.each do |s|
      assert_equal(s[1], audi.seats[s[0]], 'Seats Scheme Failed.')
    end
  end

  def test_auditorium_should_initiate_with_given_price_scheme
    seats = [['A', 9], ['B', 6], ['C', 7]]
    pricing = { 'Platinum': 320, 'Gold': 280, 'Silver': 240 }.freeze
    audi = ::Auditorium.new(seats, pricing, 1)

    assert_equal(pricing, audi.pricing, 'Pricing Scheme Failed.')
  end
end
