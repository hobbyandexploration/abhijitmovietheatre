# frozen_string_literal: true

# seat validation helper for Auditorium
module AuditoriumValidationHelper
  def out_of_range_seat_number?(seat_choice)
    invalid = false

    seat_choice.each do |sc|
      category = category_of_seat(sc)
      position = position_of_seat(sc)

      # invalid if unknown category
      invalid = true && break unless seats[category]

      category_limit = seats[category]
      # invalid if seat number is out of range
      invalid = true if position > category_limit || position <= 0
    end

    invalid
  end

  def missing_position_number?(seat_choice)
    invalid = false

    # invalid if only characters and no numbers for position.
    seat_choice.each { |sc| invalid = true unless position_of_seat(sc) }
    # return here because checking if its number on next section
    # which required number be present

    invalid
  end

  def missing_category?(seat_choice)
    invalid = false

    # invalid if its just a number
    seat_choice.each { |sc| invalid = true if is_number?(sc) }

    invalid
  end

  def wrong_number?(seat_choice)
    invalid = false

    # invalid if position characters are not number and if its zero
    seat_choice.each { |sc| invalid = true if position_of_seat(sc) <= 0 }

    invalid = true if out_of_range_seat_number?(seat_choice)

    invalid
  end

  def invalid_seat_category?(seat_choice)
    invalid = false

    # invalid if unknown category
    # also invalid, if wrong character
    requested_categories = []
    seat_choice.collect { |sc| requested_categories << category_of_seat(sc) }

    invalid = true unless (requested_categories.uniq - seats.keys).empty?

    invalid
  end

  def duplicate_seat_numbers?(seat_choice)
    return true if seat_choice.uniq.length != seat_choice.length

    false
  end

  def checks_failed(seat_choice)
    seat_choice.empty? ||
      missing_position_number?(seat_choice) ||
      missing_category?(seat_choice) ||
      wrong_number?(seat_choice) ||
      invalid_seat_category?(seat_choice) ||
      duplicate_seat_numbers?(seat_choice)
  end

  def invalid_seats(seat_choice, theatre_warnings)
    invalid = false

    invalid = true if checks_failed(seat_choice)
    theatre_warnings << '** Please type correct seat numbers **' if invalid
    invalid
  end
end
