# frozen_string_literal: true

# Seat theatre_warnings helper for Auditorium
module AuditoriumBookingHelper
  def seats_in_category_not_available?(seat_choice, theatre_warnings)
    reset_requested_seats
    categorywise_requested_seats(seat_choice)

    invalid = false

    requested_seats.each do |k, v|
      next if v <= available_seats[k]

      theatre_warnings << "#{v} Seats in #{category(k)} category are not available."
      invalid = true
    end

    invalid
  end

  def any_or_all_seats_are_booked?(seat_choice, theatre_warnings)
    return false unless booked_seats.any? { |s| seat_choice.include? s }

    reserved_seats = booked_seats & seat_choice
    theatre_warnings << "** Seats #{reserved_seats.join(', ')} are already booked"\
                       '. ** Please Select another seats.'

    true
  end

  def require_more_seats_than_available?(number, theatre_warnings)
    return false if number <= available_seats['total']

    reserved_seats = seat_choice - booked_seats
    theatre_warnings << "** #{reserved_seats.join(', ')} are already booked. **"\
                       'Please Select another seats.'

    true
  end

  def seats_not_available(number, seat_choice, theatre_warnings)
    return true if require_more_seats_than_available?(number, theatre_warnings) ||
                   seats_in_category_not_available?(seat_choice, theatre_warnings) ||
                   any_or_all_seats_are_booked?(seat_choice, theatre_warnings)

    false
  end

  def update_total_count_of_available_seats
    total = available_seats['total'] = 0
    available_seats.values.each { |v| total += v }
    available_seats['total'] = total
  end

  def mark_seats_as_booked(seat_choice)
    reset_requested_seats
    categorywise_requested_seats(seat_choice)

    requested_seats.each { |k, v| available_seats[k] -= v }

    update_total_count_of_available_seats
  end
end
