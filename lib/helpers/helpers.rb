# frozen_string_literal: true

# Helper Methods for MovieTheatre
module Helpers
  SEAT_REGEX = /([[:alpha:]]+)(\d+)/

  def category(key)
    MovieTheatre::CATEGORY[key.to_sym]
  end

  def is_number?(string)
    return false unless string

    string = string.to_s
    no_commas = string.gsub(',', '')
    matches = no_commas.match(/-?\d+(?:\.\d+)?/)
    return true if !matches.nil? && matches.size == 1 && matches[0] == no_commas

    false
  end

  def category_of_seat(seat)
    category = seat.scan(SEAT_REGEX).flatten
    return false unless category[0]

    category[0].gsub(/\s+/, '')
  end

  def position_of_seat(seat)
    pos = seat.scan(SEAT_REGEX).flatten
    return false unless pos[1]

    pos[1].to_i
  end

  def reset_requested_seats
    requested_seats.keys.each { |k| requested_seats[k] = 0 }
  end

  def categorywise_requested_seats(seat_choice)
    seat_choice.each { |sc| requested_seats[category_of_seat(sc)] += 1 }
  end
end
