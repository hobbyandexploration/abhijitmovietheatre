# frozen_string_literal: true

require 'movie_theatre/version'
require 'movie_theatre/auditorium'
require 'movie_theatre/tickets_counter'

# Manage Movie Theatre
module MovieTheatre
  CATEGORY = { 'A': 'Platinum',
               'B': 'Gold',
               'C': 'Silver' }.freeze
  # Assumption: There are only three screens
  # and each screen has seats as described in PDF Document.
  # Show 1 in Audi 1, Show 2 in Audi 2 and Show 3 in Audi 3.
  # This is a input of seating scheme for each auditorium.
  # Movie theatre assigs this to each auditorium.
  AUDI1_SEATS = [['A', 9], ['B', 6], ['C', 7]].freeze
  AUDI2_SEATS = [['A', 7], ['B', 6], ['C', 7]].freeze
  AUDI3_SEATS = [['A', 5], ['B', 8], ['C', 9]].freeze
  # Each Auditorium has pricing for its current Movie and
  # it will change with each movie.
  # But as per PDF Doc, pricing is same for all Audis as of now.
  # All Prices are in INR.
  PRICING = { 'Platinum': 320,
              'Gold': 280,
              'Silver': 240 }.freeze

  # shows to auditorium mapping
  @shows = @warnings = []
  @total_sale = { 'sub_total': 0 }
  TicketsCounter::TAXES.keys.each { |k| @total_sale[k] = 0 }
  user_is_working = true

  def self.welcome_message
    "\nWelcome To\nAbhijit Entertainment\n---\n\n"
  end

  def self.choices_list
    "1. Book Ticket.\n2. Show Total Revenue."
  end

  def self.show_warnings
    puts @warnings unless @warnings.empty?
    @warnings = [] # reset
  end

  # Movie Theatre does the initial setup of Auditoriums.
  def self.setup_auditoriums
    # Create three auditoriums. Assuming their seat arrangment from PDF Doc.
    audi1 = Auditorium.new(AUDI1_SEATS, PRICING, '1')
    audi2 = Auditorium.new(AUDI2_SEATS, PRICING, '2')
    audi3 = Auditorium.new(AUDI3_SEATS, PRICING, '3')
    @shows = [audi1, audi2, audi3]
  end

  # wrong selection of main choices
  def self.wrong_choice(choice)
    return true unless [1, 2].include? choice

    false
  end

  # wrong selection of Shows.
  def self.wrong_show_choice(choice)
    return true unless (1..3).include? choice

    false
  end

  def self.book_tickets
    show_warnings
    puts 'Enter Show Number: '
    num = gets.to_i
    @warnings << '** ERROR! Invalid Show Selection **' && return if wrong_show_choice(num)

    auditorium = @shows[num - 1]
    success = @ticket_counter.book_tickets(auditorium, @warnings)
    return false unless success

    @ticket_counter.calculate_cost(auditorium)
    @ticket_counter.show_receipt
    true
  end

  def self.calculate_total_sale
    @shows.each do |audi|
      audi_sale =  audi.send(:total_sale)

      @total_sale.keys.each do |item|
        @total_sale[item.to_sym] += audi_sale[item.to_sym]
      end
    end
  end

  def self.show_total_sale
    puts "---\n\n"
    @total_sale.each do |k, v|
      item = k.to_s.split('_').map(&:capitalize).join(' ')

      puts "#{item}: Rs. #{format('%.2f', v)}/-"
    end

    true
  end

  def self.handle_choice(choice)
    if choice == 1
      book_tickets
    elsif choice == 2
      calculate_total_sale
      show_total_sale
    end
  end

  Kernel.trap('SIGINT') { throw :ctrl_c }
  catch :ctrl_c do
    begin
      puts 'Please Wait. Initializing for first time.'
      @ticket_counter = TicketsCounter.new
      setup_auditoriums

      while user_is_working
        puts welcome_message
        puts choices_list

        puts 'Enter Selection: '
        puts '(Ctrl+C to Exit)'

        show_warnings
        choice = gets.to_i
        @warnings << '** ERROR! Invalid Selection **' && next if wrong_choice(choice)

        completed = false
        completed = handle_choice(choice) until completed
      end
    rescue StandardError => e
      puts "ERROR! #{e}"
    end
  end

  # Exit bin/console prompt too.
  exit
end
