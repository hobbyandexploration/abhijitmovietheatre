# frozen_string_literal: true

require 'helpers/helpers'
require 'helpers/auditorium_validation_helper'
require 'helpers/auditorium_booking_helper'
require_relative 'tickets_counter'

# Auditorium defines seats, movie to run, pricing and available seats.
class Auditorium
  include Helpers
  include AuditoriumValidationHelper
  include AuditoriumBookingHelper

  attr_accessor :seats, :available_seats, :requested_seats,
                :booked_seats, :total_sale, :pricing

  def initialize(seat_scheme, pricing, movie)
    @seats = {}
    seat_scheme.each { |s| @seats[s[0]] = s[1] }

    @available_seats = @seats.dup
    update_total_count_of_available_seats

    @requested_seats = @seats.dup
    reset_requested_seats
    @booked_seats = []

    @movie = movie
    @pricing = pricing

    @total_sale = { 'sub_total': 0 }
    TicketsCounter::TAXES.keys.each { |k| @total_sale[k] = 0 }
  end

  def show_available_seats
    @seats.each do |k, v|
      row = []
      (1..v).each do |s|
        seat = "#{k}#{s}"
        available_seat = (@booked_seats.include? seat) ? '' : seat
        row << available_seat
      end
      p row
    end
  end

  def reserve_seats(theatre_warnings)
    puts 'Enter Seats to Book (e.g. A1, A2): '
    seat_choice = gets.chomp.split(',')
    number = seat_choice.length
    # remove empty spaces
    seat_choice.collect!(&:strip)

    return false if invalid_seats(seat_choice, theatre_warnings)
    return false if seats_not_available(number, seat_choice, theatre_warnings)

    @booked_seats += seat_choice
    puts "Successfully booked #{seat_choice.join(', ')} for Show #{@movie}."

    mark_seats_as_booked(seat_choice)
    true
  end

  def record_sale(last_transaction)
    @total_sale.keys.each do |item|
      @total_sale[item.to_sym] += last_transaction[item.to_sym]
    end
  end
end
