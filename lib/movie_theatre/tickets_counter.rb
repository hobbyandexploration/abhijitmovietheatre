# frozen_string_literal: true

# Handle Ticket Booking for each Auditorium
class TicketsCounter
  TAXES = { 'service_tax': 14,
            'swachh_bharat_cess': 0.5,
            'krishi_kalyan_cess': 0.5 }.freeze

  attr_accessor :warnings

  def initialize
    @warnings = []
    @last_transaction = {}
  end

  def show_warnings
    @warnings.each { |w| puts w } unless @warnings.empty?
    @warnings = []
  end

  def reset_last_transaction
    @last_transaction.keys.each { |k| @last_transaction[k] = 0 }
  end

  def book_tickets(auditorium, theatre_warnings)
    # reset last transaction of booking
    reset_last_transaction
    auditorium.reset_requested_seats

    auditorium.show_available_seats
    show_warnings

    return false unless auditorium.reserve_seats(theatre_warnings)

    true
  end

  def calculate_taxes
    sub_total = @last_transaction[:sub_total]

    TAXES.each do |tax_type, amount|
      tax = (sub_total / 100.0 * amount).round(2)
      @last_transaction[tax_type] = tax
    end
  end

  def calculate_total
    @last_transaction[:total] = 0

    @last_transaction.values.each do |amount|
      @last_transaction[:total] += amount
    end
  end

  def calculate_cost(auditorium)
    auditorium.requested_seats.each do |seat_type, number_of_seats|
      next if number_of_seats.zero?

      category = MovieTheatre::CATEGORY[seat_type.to_sym]
      price = MovieTheatre::PRICING[category.to_sym]

      sub_total = price * number_of_seats
      @last_transaction[:sub_total] = sub_total

      calculate_taxes
      calculate_total
      auditorium.record_sale(@last_transaction)
    end
  end

  def show_receipt
    puts '---'
    @last_transaction.each do |k, v|
      tax = TAXES[k] || ''
      tax_rate = "@#{tax}%" unless tax.to_s.empty?

      item = k.to_s.split('_').map(&:capitalize).join(' ')
      item = "#{item} #{tax_rate}" if tax_rate

      puts "#{item}: Rs. #{format('%.2f', v)}/-"
    end
    puts '---'
  end
end
